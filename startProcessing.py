# -*- coding: utf-8 -*-

import os
import sys
import logging
import hashlib
import time
from datetime import datetime
from multiprocessing.dummy import Pool as ThreadPool
import argparse

from classes.FileHandler import FileHandler
from classes.FilesHashes import FilesHashes
from classes.FileWriter import FileWriter


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dir', nargs='?', default='.\\')  # work dir
    parser.add_argument('-t', '--threads', nargs='?', default=8, type=int)  # number of threads
    parser.add_argument('-r', '--res_dir', nargs='?', default='.\\result\\')  # dir for writing result files
    parser.add_argument('--hashed', nargs='?', default=True, type=bool)  # hashing for files
    return parser


def md5_checksum(file_path):
    with open(file_path, 'rb') as fh:
        m = hashlib.md5()
        while True:
            data = fh.read(8192)
            if not data:
                break
            m.update(data)
        return m.hexdigest()


if __name__ == '__main__':
    try:
        try:
            os.mkdir(u"./logs/")
        except OSError:
            if not os.path.isdir(u"./logs/"):
                raise
        logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
                            level=logging.DEBUG, filename=u'./logs/log_%s.log' % datetime.now().strftime('%Y.%m.%d %H-%M'))
        my_parser = create_parser()
        namespace = my_parser.parse_args(sys.argv[1:])
        pool = ThreadPool(processes=namespace.threads)  # start worker processes
        file_writer = FileWriter(namespace.dir, namespace.res_dir)
        file_handler = FileHandler(file_writer, namespace.hashed)
        start_time = time.time()
        existing_file_path = None
        file_size = 0
        hash_sum = ''
        # Сканирование с помощью функции os.walk короче и проще нежели стандартное решение в виде рекурсивной функции
        for path, dirs, files in os.walk(namespace.dir):
            # path - текущий каталог куда смотрит цикл (строка)
            # dirs - массив имен директорий в текущем каталоге
            # files - массив имен файлов в текущем каталоге
            for fname in files:
                file_format = fname.split('.')[-1]
                if file_format in file_handler.FORMATS:
                    file_path = os.path.join(path, fname).decode('cp1251')  # windows!!
                    if namespace.hashed:
                        file_size = os.path.getsize(file_path)
                        hash_sum = md5_checksum(file_path)
                        # check if file already processed (contains in FilesHashes)
                        existing_file_path = FilesHashes.get_file_path(hash_sum, file_size)
                    if existing_file_path:
                        logging.info("File was founded in hash table: %s", file_path)
                        # copy existing file content
                        file_writer.copy_file(file_path, existing_file_path)
                    else:
                        # start processing
                        logging.info("Start processing file: %s", file_path)
                        pool.apply_async(file_handler.process_file, (file_path, hash_sum, file_size, ))
        pool.close()  # that's all files
        pool.join()  # w8 until all items in the queue have been gotten and processed
        print "Completed, see log file for details"
        writen, copied = file_writer.get_files_statistic()
        print "Count of recorded files: ", writen
        print "Count of copied files: ", copied
        print("--- Total time: %s seconds ---" % (time.time() - start_time))
    except SystemError, e:
        logging.critical(e.message)
        raise SystemExit(-2)
    except Exception, e:
        logging.critical(e.message)
        print e
        raise SystemExit(-1)
