# -*- coding: utf-8 -*-


import subprocess
import re
import os, errno
import logging

from classes.FilesHashes import FilesHashes


class FileHandler:

    FORMATS = ['cpp']

    def __init__(self, file_writer, hashed=False):
        self.file_writer = file_writer
        self.hashed = hashed  # hashing flag

    def process_file(self, file_path, hash_sum='', file_size=0):
        """
        remove c-style comments.
        hash_sum & file_size needed for hashing
        """
        try:
            with open(file_path, 'r') as input_file:
                # correct for windows
                try:
                    #raise Exception("Test exp")
                    process = subprocess.Popen(["sed", "-n", "-f", 'remccoms3.sed'],
                        stdin=input_file, stdout=subprocess.PIPE)
                    cmd_out, cmd_err = process.communicate()
                    stripped_code = cmd_out
                except Exception, e:
                    # try to solve with local function with regex
                    logging.warning("Can't process file with [sed]: %s", file_path)
                    try:
                        contents = input_file.read()
                        stripped_code = self.remove_comments_with_script(contents)
                    except Exception, e:
                        logging.error("Can't process file with [script]: %s", file_path)
                        stripped_code = None
            if stripped_code:
                # save result to the file
                cleaned_text = '\n'.join(filter(str.strip, stripped_code.splitlines()))
                new_file_path = self.file_writer.write_file_data(file_path, cleaned_text)
                # save result to the hash
                if self.hashed:
                    try:
                        FilesHashes.add_file(hash_sum, new_file_path, file_size)
                    except TypeError, e:
                        print e
            else:
                logging.error("Can't process file : %s", file_path)
        except EnvironmentError:  # parent of IOError, OSError *and* WindowsError where available
            logging.error("Error while opening the file : %s", file_path)

    def remove_comments_with_script(self, text):

        def replacer(match):
            s = match.group(0)
            if s.startswith('/'):
                return " "  # note: a space and not an empty string
            else:
                return s

        pattern = re.compile(
            r'//.*?$|/\*.*?\*/|\'(?:\\.|[^\\\'])*\'|"(?:\\.|[^\\"])*"',
            re.DOTALL | re.MULTILINE
        )
        return re.sub(pattern, replacer, text)
