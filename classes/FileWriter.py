# -*- coding: utf-8 -*-

from multiprocessing.dummy import Value
import os
import errno
import shutil
import logging


class FileWriter:
    def __init__(self, work_dir, result_dir):
        self.work_dir = work_dir
        self.result_dir = result_dir
        self.writen_files = Value('i', 0)
        self.copied_files = Value('i', 0)

    def write_file_data(self, file_path, file_data):
        #print self.result_dir
        #print self.get_cur_file_dest_dir(file_path)
        new_file_path = os.path.join(self.result_dir, self.get_cur_file_dest_dir(file_path))
        #print new_file_path
        logging.info("Write file: %s", new_file_path)
        self.check_folder(new_file_path)
        with open(new_file_path, 'w') as output_file:
            output_file.write(file_data)
            self.writen_files.value += 1
        return new_file_path

    def copy_file(self, file_path, existing_file_path):
        new_file_path = os.path.join(self.result_dir, self.get_cur_file_dest_dir(file_path))
        logging.info("Copy file: %s", new_file_path)
        self.check_folder(new_file_path)
        shutil.copy2(existing_file_path, new_file_path)
        self.copied_files.value += 1
        return new_file_path

    def get_cur_file_dest_dir(self, file_path):
        return file_path[len(self.work_dir):]

    def check_folder(self, file_path):
        file_dir = os.path.dirname(file_path)
        try:
            if not os.path.exists(file_dir):
                os.makedirs(file_dir)
        except OSError as e:
            if e.errno != errno.EEXIST:  # if another thread create it firsts - its ok
                logging.error("Error during writing the file: %s", file_path)

    def get_files_statistic(self):
        return self.writen_files.value, self.copied_files.value
