# -*- coding: utf-8 -*-


class FilesHashes:
    """
        Files info container for processing duplicates
    """

    hash = {}  # key - file_hash, value - (file_path, file_size) (list of file_paths)

    @staticmethod
    def add_file(file_hash, file_path, file_size):
        """
        Add info about processed file to the container
        :param str file_hash: md5 hash of ORIGINAL file
        :param str file_path: path of RESULTING file
        :param int file_size: size of ORIGINAL file to prevent collisions
        """
        if FilesHashes.check_file_is_exist(file_hash, file_size):
            FilesHashes.hash[file_hash].append((file_path, file_size))
        else:
            FilesHashes.hash[file_hash] = [(file_path, file_size)]
        #print FilesHashes.hash

    @staticmethod
    def check_file_is_exist(file_hash, file_size):
        if file_hash in FilesHashes.hash:
            for values in FilesHashes.hash[file_hash]:
                if values[1] == file_size:
                    return True
            return False
        else:
            return False

    @staticmethod
    def get_file_path(file_hash, file_size):
        if file_hash in FilesHashes.hash:
            for values in FilesHashes.hash[file_hash]:
                if values[1] == file_size:
                    return values[0]
            return None
        else:
            return None
